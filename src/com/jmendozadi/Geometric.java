package com.jmendozadi;

public abstract class Geometric {

    public String figurename;
    public int sidesnumber;
    public double side;
    public double diameter;
    public static double area;
    public static double perimeter;
    public static double radius;

    public Geometric(String figurename,int sidesnumber,double side) {
        this.figurename = figurename;
        this.sidesnumber = sidesnumber;
        this.side = side;
        this.diameter = diameter;

    }

    public abstract void areaCalculation();
    public abstract void perimeterCalculation();
    public abstract void perimeterCalculationTwo();
    public abstract void  radiusCalculation();
    public abstract double areaCalculatorWithRope(double rope, double distanceCenter);//Bryson Florez - Agrego método
}
